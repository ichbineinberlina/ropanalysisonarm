<?php

require_once('dom_test.inc');

$XMLStringGood = file_get_contents('http://www.php.net/');

$dom = new DOMDocument;
$dom->resolveExternals = TRUE;

$dom->validateOnParse = FALSE;
echo "validateOnParse set to FALSE: \n";
$dom->loadXML($XMLStringGood);
echo "No Error Report Above\n";

$BogusElement = $dom->createElement('NYPHP','DOMinatrix');
$Body = $dom->getElementsByTagName('body')->item(0);
$Body->appendChild($BogusElement);
$XMLStringBad = $dom->saveXML();

echo "validateOnParse set to TRUE: \n";
$dom->validateOnParse = TRUE;
$dom->loadXML($XMLStringBad);
echo "Error Report Above\n";

?>
