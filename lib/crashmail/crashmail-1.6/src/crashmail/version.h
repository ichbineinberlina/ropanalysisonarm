#ifndef VERSION_H
#define VERSION_H

#define VERSION_MAJOR 1
#define VERSION_MINOR 5
#define VERSION_PATCH 0

#define VERSION "1.6"

#define TID_VERSION "1.6"

#endif /* VERSION_H */

