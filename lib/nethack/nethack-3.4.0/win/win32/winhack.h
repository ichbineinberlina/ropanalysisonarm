/*	SCCS Id: @(#)winhack.h	3.4	2002/03/06	*/
/* Copyright (c) Alex Kompel, 2002                                */
/* NetHack may be freely redistributed.  See license for details. */


#if !defined(AFX_WINHACK_H__6397C328_BAF8_460C_9465_F12C596C5732__INCLUDED_)
#define AFX_WINHACK_H__6397C328_BAF8_460C_9465_F12C596C5732__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define STRICT 
#include <windows.h>
#include "resource.h"


#endif // !defined(AFX_WINHACK_H__6397C328_BAF8_460C_9465_F12C596C5732__INCLUDED_)
