#!/bin/bash

# 
file="events_raw_$1"
# create instance file with defined name if it does not exist; else: clear file
if [ ! -f ../data/events/$file ]; then
    touch ../data/events/$file
else 
    true > ../data/events/$file
fi

#
file_instructions="events_raw_$1_1"
# create file for instructions:u with defined name if it does not exist; else: clear file
if [ ! -f ../data/events/$file ]; then
    touch ../data/events/$file_instructions
else 
    true > ../data/events/$file_instructions
fi

file_other_events="events_raw_$1"
# create instance file for all attributes without instructions with defined name if it does not exist; else: clear file
if [ ! -f ../data/events/other_events/$file_other_events ]; then
    touch ../data/events/other_events/$file_other_events
else 
    true > ../data/events/other_events/$file_other_events
fi

# 
file_sum="events_raw_$1"
# create instance file for instructions:u sum with defined name if it does not exist; else: clear file
if [ ! -f ../data/events/instructions_sum/$file_sum ]; then
    touch ../data/events/instructions_sum/$file_sum
else 
    true > ../data/events/instructions_sum/$file_sum
fi

# use the last value of every line (which is the time passed since activation of a hardware event) to sort the instances
awk '{print($NF" "$0)}' ../data/events/events_raw | sort -k1,1 -n -t' ' | cut -f2- -d' ' &> ../data/events/tmp
# get all values of every line except the last value of every line, because the time is not needed anymore
grep -Po '.*(?=\s+[^\s]+$)' ../data/events/tmp &> ../data/events/tmp2
mv ../data/events/tmp2 ../data/events/events_raw

# get the type and config of the first hardwae event of the last line
type="$(tail -n1 ../data/events/events_raw | cut -d' ' -f1)"
config="$(tail -n1 ../data/events/events_raw | cut -d' ' -f2)"

# the implementation needs instructions:u as the first hardware event in the instances
# if cpu-cycles is in the first place we need to move it to the end
if [[ "$type" -eq 0 && "$config" -eq 0 ]]; then
    touch ../data/events/instructions_tmp_cycle
    touch ../data/events/instructions_tmp_other
    # extract the type,config and delta of cpu cycles
    (cut -d' ' -f1-3 ../data/events/events_raw) &> ../data/events/instructions_tmp_cycle   
    (cut -d' ' -f4- ../data/events/events_raw) &> ../data/events/instructions_tmp_other
    # paste cpu-cycles to the end of every line of the instances
    (pr -mts' ' ../data/events/instructions_tmp_other ../data/events/instructions_tmp_cycle) &> ../data/events/events_raw
    rm -f ../data/events/instructions_tmp_cycle
    rm -f ../data/events/instructions_tmp_other
fi
# delete type and config from instructions:u and extract the remainder to events_raw_$1
(colrm 1 4 < ../data/events/events_raw) &> ../data/events/$file
# extract the delta of instructions:u to events_raw_$1_1
(cut -d ' ' -f 1 < ../data/events/$file) &> ../data/events/$file_instructions
# delete instructions:u in events_raw_$1 and save remainding events to events_raw_$1 in other_events
(sed 's/[^ ]* //' ../data/events/$file) &> ../data/events/other_events/$file_other_events

sum=0
#iterate through the lines of events_raw_$1_1 and read the value of the line every time
while read num
do 
    # add the value of the current line to the global sum of all lines
	((sum += num))
    # save the current global sum to events_raw_$1_1 in instructions_sum
	echo $sum >>../data/events/instructions_sum/$file_sum
done < ../data/events/$file_instructions
# delete content of events_raw for later measurements
true > ../data/events/events_raw

