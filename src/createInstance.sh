#!/bin/bash


create () {

NUMBERS=()
NAMES=()
for filename in ../data/events/instructions_sum/*; do
    # get all files in directory and their first value in the first line
    name=$(basename -- "$filename")
	num=$(head -n 1 $filename)
    # add file names to NAMES and the values to NUMBERS
    NAMES+=($name)
	NUMBERS+=($num)
done

file_name=${NAMES[0]}
min=${NUMBERS[0]}
count=0
# find the smallest value in the list NUMBERS
for i in "${NUMBERS[@]}"; do
  (( count++ ))
	if [ $i -lt $min ]  
	then
		min=$i
        index_name=$((count-1))
        file_name=${NAMES[$index_name]}
	fi 
done
err=0
# check for all entries in the list NUMBERS if the difference between them and smallest entry is greater than 75
for i in "${NUMBERS[@]}"; do
	difference=$(( i-min ))
	if [ $difference -gt 75 ]  
	then
        # if difference is greater than 75 we cannot create an instance
		err=1
	fi
done
# if an error occured we delete the smallest value since the difference is to big
if [ $err -eq 1 ]  
then
    # delete first line of the file with contains the smallest value
    # delete first line of the file with the same name in director other_events because this instance is related to the smallest value
	filename="$file_name"
	sed -i '1d' ../data/events/instructions_sum/$filename
	sed -i '1d' ../data/events/other_events/$filename
else
    # instances begin with  the class of the instance (1=ROP, -1=normal)
	instance="$type "
    # delete all first lines in instructions_sum because we can create an instance and values shouldn't be used twice for instance
	for filename in ../data/events/instructions_sum/*; do
		sed -i '1d' $filename
	done
	for filename in ../data/events/other_events/*; do
        # get attributes with values of each file and use them for the instance
		instance="$instance $(head -n 1 $filename)"
        # delete those values in the first line
		sed -i '1d' $filename
	done
    # save instance in train
    echo "$instance" >> ../data/instances/train
fi

NAMES=()
NUMBERS=()

}
empty=0
# check if at least one file in instructions_sum is empty
for filename in ../data/events/instructions_sum/*; do
	if ! [ -s $filename ]
	then
        	empty=1
	fi
done
type="$1"

# if at least one file is empty we cannot create an instance
# do nothing in this case
while [ $empty -eq 0 ]
do
    # if we can create instances call create
	create
    # check if at least one file is empty
   	for filename in ../data/events/instructions_sum/*; do
		if ! [ -s $filename ]
	then
        	empty=1
	fi
	done
done

