#!/bin/bash
# use arguments 1-2 (C=$1 for not scaled datased; gamma=$2 for not scaled dataset) for learning svm model with dataset
../tools/libsvm/svm-train -c $1 -g $2 ../data/instances/formatted_train
# use arguments 3-4 (C=$3 for scaled datased; gamma=$4 for scaled dataset) for learning svm model with scaled dataset
../tools/libsvm/svm-train -c $3 -g $4 ../data/instances/scale_formatted_train
