#!/bin/bash

# iterate over all lines of input file
while read str
do 
    start=0
    attribut_num=1
    # get the numbers of words
    num_words=$(wc -w <<< "$str")
    iter=2
    # the new string begins with class of instance
    new_str="$(cut -d' ' -f1 <<<\"$str\")"
    str="$(echo $str)"
    # iterate over all words of the line
    while [ "$iter" -le "$num_words" ]
        do
                # get the current word of the line
                new="$(cut -d' ' -f$iter <<<\"$str\")"
                
                # every third value of the line will be extracted since it is the delta value
                let "start +=1"
                if [ $start -eq 3 ]
                then
                    # extract the delta value and concatenate it with the previous delta values.
                    # the new instance has the format: class 1:delta1 2:delta2 3:delta3 ...
                    sub="$(cut -d' ' -f$iter <<<\"$str\")"
                    new_str="$new_str $attribut_num:$sub"
                    let "attribut_num +=1"
                    let "start =0"
                fi
                let "iter +=1"

        done
    # remove possible " characters
    new_str="${new_str//\"}"
    # save the new instance in formatted_$1
    echo "$new_str" >> /home/pi/Desktop/hpconarm/data/instances/formatted_$1     
done < /home/pi/Desktop/hpconarm/data/instances/$1







