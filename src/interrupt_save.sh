#!/bin/bash

# create lock for exclusive write access
(
  flock -e -x 200
  # save instance from interrupt handler to events_raw
  echo "$*" >> /home/pi/Desktop/hpconarm/data/events/events_raw
) 200< /home/pi/Desktop/hpconarm/data/events/events_raw
# delete lock