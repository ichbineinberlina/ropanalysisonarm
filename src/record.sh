#!/bin/bash
name="$(./wifirxHPC)"


if ! [ "$1" -eq "$1" ] 2> /dev/null || ! [ "$2" -eq "$2" ] 2> /dev/null 
then
    echo "Input must be two integer"
    exit 1
fi
if [ $1 -gt 100 ] || [ $1 -lt 0 ]  
then
	echo "Input must be integer between 0 and 100"
	exit 1
fi
# define split ratio
let "num_non_exploit = 1000 - ($1 * 10)"
let "num_exploit = 1000 - $num_non_exploit"
#define frequency of perf
let "frequency = $2"

exploit_list=( "$(./wifirxHPC)" "/etc/group" "/etc/hosts" )
#non_exploit_list=
