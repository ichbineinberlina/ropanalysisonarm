#!/bin/bash

# create write lock for events_raw
(
    flock -e -x 200
    # save instance in events_raw
    echo "$*" >> /home/pi/Desktop/hpconarm/data/events/events_raw
    touch /home/pi/Desktop/hpconarm/data/instances/testInstance
    # save instance in testInstance with default class 1 since LIBSVM always needs class
    echo "1 $*" >  /home/pi/Desktop/hpconarm/data/instances/testInstance
    cp /dev/null /home/pi/Desktop/hpconarm/data/instances/formatted_testInstance
    # format instance for libsvm
    bash /home/pi/Desktop/hpconarm/bin/formatModel.sh testInstance
    # insert instance in training data for scaling 
    cat /home/pi/Desktop/hpconarm/data/instances/formatted_testInstance >> /home/pi/Desktop/hpconarm/data/instances/formatted_train
    sed -i '/^\s*$/d' /home/pi/Desktop/hpconarm/data/instances/formatted_train
    cp /dev/null /home/pi/Desktop/hpconarm/data/instances/scale_testInstance
    # scale training data and test instance
    /home/pi/Desktop/hpconarm/tools/libsvm/svm-scale -l -1 -u 1 /home/pi/Desktop/hpconarm/data/instances/formatted_train &> /home/pi/Desktop/hpconarm/data/instances/all_scale
    # extract scaled test instance from scaled data
    tail -n 1 /home/pi/Desktop/hpconarm/data/instances/all_scale &> /home/pi/Desktop/hpconarm/data/instances/scale_testInstance
    # predict class of test instance and scaled test instance and save result in result and scale_result
    /home/pi/Desktop/hpconarm/tools/libsvm/svm-predict /home/pi/Desktop/hpconarm/data/instances/formatted_testInstance /home/pi/Desktop/hpconarm/tools/libsvm/formatted_train.model /home/pi/Desktop/hpconarm/data/evaluation/result
    /home/pi/Desktop/hpconarm/tools/libsvm/svm-predict /home/pi/Desktop/hpconarm/data/instances/scale_testInstance /home/pi/Desktop/hpconarm/tools/libsvm/scale_formatted_train.model /home/pi/Desktop/hpconarm/data/evaluation/scale_result
    # save result to global results
    cat /home/pi/Desktop/hpconarm/data/evaluation/result >> /home/pi/Desktop/hpconarm/data/evaluation/result_all
    cat /home/pi/Desktop/hpconarm/data/evaluation/scale_result >> /home/pi/Desktop/hpconarm/data/evaluation/scale_result_all
) 200< /home/pi/Desktop/hpconarm/data/events/events_raw
