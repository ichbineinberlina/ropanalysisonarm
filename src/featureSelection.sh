#!/bin/bash
# call fselect.py from LIBSVM with formatted_train as argument
# formatted_train contains instances with all hardware events
python ../tools/libsvm/tools/fselect.py ../data/instances/formatted_train
