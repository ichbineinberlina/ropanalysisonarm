#!/bin/bash



#check if argument (frequency) is a positive integer
if ! [[ "$1" =~ ^[0-9]+$ ]]
    then
        echo "Frequency argument must be a positive integer"
	exit 1
fi
# check if argument is greater than 0
if [ $1 -le 0 ]  
then
	echo "Frequency argument must be an integer greater than 0"
	exit 1
fi

# define frequency of perf
let "frequency = $1"

# create or empty train and events_raw, which are used for saving the instances
if [ ! -f ../data/instances/train ]; then
    touch ../data/instances/train
else 
    true > ../data/instances/train
fi

if [ ! -f ../data/events/events_raw ]; then
    touch ../data/events_raw
else 
    true > ../data/events_raw
fi

# define exploit programs in a list for iteration
exploit_list=( "$(./wifirxHPC 0 1)" "$(./pmsHPC 0 1)" "$(./phpHPC 0 1)" "$(./dnstracerHPC 0 1)" )
# define non-exploit programs in a list for iteration
non_exploit_list=( "$(./wifirxHPC 0 0)" "$(./pmsHPC 0 0)" "$(./phpHPC 0 0)" "$(./dnstracerHPC 0 0)")

# define groups of events, which will be used for perf 
# instructions:u will be used for merging instances
event_list+=("instructions:u,cycles,branches,branch-misses,bus-cycles")
event_list+=("instructions:u,cache-misses,cache-references,L1-dcache-load-misses")
event_list+=("instructions:u,L1-dcache-loads,L1-dcache-store-misses,L1-dcache-stores")
event_list+=("instructions:u,L1-icache-load-misses,L1-icache-loads,LLC-load-misses")
event_list+=("instructions:u,LLC-loads,LLC-store-misses,LLC-stores")
event_list+=("instructions:u,branch-load-misses,branch-loads,dTLB-load-misses")
event_list+=("instructions:u,dTLB-store-misses,iTLB-load-misses,armv7_cortex_a7/br_immed_retired/")
event_list+=("instructions:u,armv7_cortex_a7/br_mis_pred/,armv7_cortex_a7/br_pred/,armv7_cortex_a7/br_return_retired/")
event_list+=("instructions:u,armv7_cortex_a7/bus_access/,armv7_cortex_a7/bus_cycles/,armv7_cortex_a7/cid_write_retired/")
event_list+=("instructions:u,armv7_cortex_a7/cpu_cycles/,armv7_cortex_a7/exc_return/,armv7_cortex_a7/exc_taken/")
event_list+=("instructions:u,armv7_cortex_a7/inst_retired/,armv7_cortex_a7/inst_spec/,armv7_cortex_a7/l1d_cache/")
event_list+=("instructions:u,armv7_cortex_a7/l1d_cache_refill/,armv7_cortex_a7/l1d_cache_wb/,armv7_cortex_a7/l1d_tlb_refill/")
event_list+=("instructions:u,armv7_cortex_a7/l1i_cache/,armv7_cortex_a7/l1i_cache_refill/,armv7_cortex_a7/l1i_tlb_refill/")
event_list+=("instructions:u,armv7_cortex_a7/l2d_cache/,armv7_cortex_a7/l2d_cache_refill/,armv7_cortex_a7/l2d_cache_wb/")
event_list+=("instructions:u,armv7_cortex_a7/ld_retired/,armv7_cortex_a7/mem_access/,armv7_cortex_a7/memory_error/")
event_list+=("instructions:u,armv7_cortex_a7/pc_write_retired/,armv7_cortex_a7/st_retired/,armv7_cortex_a7/sw_incr/")
event_list+=("instructions:u,armv7_cortex_a7/ttbr_write_retired/,armv7_cortex_a7/unaligned_ldst_retired/")


# add exit command to .profile
# sh shell is closed if rop exploit is finished -> script continues without problems
# WARNING: could limit system functions if exit command is not deleted at the end
echo "exit" >> ~/.profile
source ~/.profile &

# iterate over rop exploits
for rop in "${exploit_list[@]}"
do
		let "event_index = 1"
        # use every hardware event combination in event_list for measurement
		for event in "${event_list[@]}"
		do
            # use perf to count hardware events, which could be triggered, if the rop program executes
            # the argument is used as frequency and hardware events are taken from current iteration of event_list
			../linux/tools/perf/perf record -e $event -F $frequency $rop || true
            # calculate instructions:u sum for every instance; this is used for merging 
			sudo bash prepareRaw.sh $event_index
			let "event_index += 1"
		done
        # use instructions:u sum to create instances
		sudo bash createInstance.sh 1
done

# iterate over normal executions
for normal in "${non_exploit_list[@]}"
do
		let "event_index = 1"
        # use every hardware event combination in event_list for measurement
		for event in "${event_list[@]}"
		do
            # use perf to count hardware events, which could be triggered, if the normal program executes
			../linux/tools/perf/perf record -e $event -F $frequency $normal || true
            # calculate instructions:u sum for every instance; this is used for merging 
			sudo bash prepareRaw.sh $event_index
			let "event_index += 1"
		done
        # use instructions:u sum to create instances
		sudo bash createInstance.sh -1
        done	
done

# format instances for LIBSVM
bash formatModel.sh train

# delete command exit for normal execution
sed -i '/exit/d' ~/.profile
source ~/.profile &
