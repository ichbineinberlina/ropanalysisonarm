#!/bin/bash

#check if argument (frequency) is a positive integer
if ! [[ "$1" =~ ^[0-9]+$ ]]
    then
        echo "Frequency argument must be a positive integer"
	exit 1
fi
# check if argument is greater than 0
if [ $1 -le 0 ]  
then
	echo "Frequency argument must be an integer greater than 0"
	exit 1
fi

# define frequency of perf
let "frequency = $1"

# create or empty train and events_raw, which are used for saving the instances
if [ ! -f ../data/instances/train ]; then
    touch ../data/instances/train
else 
    true > ../data/instances/train
fi

if [ ! -f ../data/events/events_raw ]; then
    touch ../data/events_raw
else 
    true > ../data/events_raw
fi

# define exploit programs in a list for iteration
exploit_list=( "$(./wifirxHPC 0 1)" "$(./pmsHPC 0 1)" "$(./phpHPC 0 1)" "$(./dnstracerHPC 0 1)" )
# define non-exploit programs in a list for iteration
non_exploit_list=( "$(./wifirxHPC 0 0)" "$(./pmsHPC 0 0)" "$(./phpHPC 0 0)" "$(./dnstracerHPC 0 0)" )

# add exit command to .profile
# sh shell is closed if rop exploit is finished -> script continues without problems
# WARNING: could limit system functions if exit command is not deleted at the end
echo "exit" >> ~/.profile
source ~/.profile &

# use every rop exploit in a loop
for rop in "${exploit_list[@]}"
do
            # use perf to count hardware events, which could be triggered, if the rop program executes
            # the argument is used as frequency and hardware events are taken from the second argument of the script
			../linux/tools/perf/perf record -e $2 -F $frequency $rop || true    
done
# add the class 1 to the instances in events_raw
perl -i -pe '/^1 / or print "1 "' ../data/events/events_raw
# copy instances to train
sudo cp ../data/events/events_raw ../data/instances/train

# use every normal execution in a loop
for normal in "${non_exploit_list[@]}"
do
            # use perf to count hardware events, which could be triggered, if the normal program executes
			../linux/tools/perf/perf record -e $2 -F $frequency $normal || true
done
#add class -1 to events_raw
perl -i -pe '/^-1 / or print "-1 "' ../data/events/events_raw
#add new instances to the other instances
cp ../data/events/events_raw ../data/instances/train2
cat ../data/instances/train2 >> ../data/instances/train
# format instances for LIBSVM
bash formatModel.sh train

# delete command exit for normal execution
sed -i '/exit/d' ~/.profile
source ~/.profile
