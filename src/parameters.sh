#!/bin/bash
# scale training data to values between -1 and 1
../tools/libsvm/svm-scale -l -1 -u 1 ../data/instances/formatted_train &> ../data/instances/scale_formatted_train
# find optimal values for parameter C and gamma with k-fold cross validation (k is argument of script) for training data and scaled training data
python ../tools/libsvm/tools/grid.py -v $1 ../data/instances/formatted_train &> grid_train
python ../tools/libsvm/tools/grid.py -v $1 ../data/instances/scale_formatted_train &> grid_scale_train
